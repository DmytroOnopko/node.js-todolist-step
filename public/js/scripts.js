document.addEventListener("DOMContentLoaded", (e) => {
    removeNotes = () => {
        let btnDelete = document.getElementsByClassName("btn-delete");
        for (let i = 0; i < btnDelete.length; i++) {
            btnDelete[i].addEventListener("click", () => {
                const id = btnDelete[i].closest(".col-3").id;
                fetch(`/api/notes/${id}`, {
                    method: 'DELETE'
                }).then(
                    removeElement(id)
                )})
        }
    };
    removeNotes();

    showNotes = () => {
        let btnEdit = document.getElementsByClassName("btn-edit");
        for (let i = 0; i < btnEdit.length; i++) {
            btnEdit[i].addEventListener("click", () => {
                const id = btnEdit[i].closest(".col-3").id;
                fetch(`/api/notes/${id}`).then(res => {
                    return res.text();
                }).then((text) => {
                    document.getElementById('modal').innerText = text;
                })
            })
        }
    };
    showNotes();
});

removeElement = (id) => {
    let elem = document.getElementById(id);
    elem.parentNode.removeChild(elem);
};
