const ObjectId = require('mongodb').ObjectId;

module.exports = (app, db) => {
    // Роут GET /api/notes/${id} для отображения заметки.
    app.get('/api/notes/:id', async(req, res) => {
            try{
                res.render('editNotes', {result: await db.collection('ToDoListCollection').findOne({_id: ObjectId(req.params.id)})})
            }catch (e) {
                console.log(e);
                return e;
            }
    })
};