module.exports = (app, db) => {
    //Роут GET /, который будет отдавать HTML страницу с формой создания заметки.
    app.post('/', (req, res) => {
        try {
            db.collection('ToDoListCollection')
                .insertOne({
                    type: 'note',
                    title: req.body.title,
                    description: req.body.description
                });
        } catch (err) {
            console.log(err);
            return err;
        }
        res.send('notes added');
        console.log('notes added');
    });
};
