const ObjectId = require('mongodb').ObjectId;
module.exports = function (app, db) {
    //Роут DELETE /api/notes/${id} для удаления заметки.
    app.delete ('/api/notes/:id', (req, res) => {
        try{
            db.collection('ToDoListCollection')
                .findOneAndDelete({
                    _id: ObjectId(req.params.id)
                });
        }catch (err) {
            console.log(err);
            return err;
        }
        res.send('notes deleted');
        console.log('notes deleted');
    });
};