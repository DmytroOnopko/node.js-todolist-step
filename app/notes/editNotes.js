const ObjectId = require('mongodb').ObjectId;

module.exports = (app, db) => {
    // Роут PUT /api/notes/${id} для редактирования заметки.
    app.put('/api/notes/:id', (req, res) => {
        try {
            db.collection('ToDoListCollection').updateOne(
                {'_id': ObjectId(req.params.id)},
                {
                    $set: {
                        'title ': req.body.title,
                        "description": req.body.description
                    }
                },
                {upsert: true});
        } catch (err) {
            console.log(err);
            return err;
        }
        res.send('note updated');
        console.log("note updated")
    })
};