module.exports = (app, db) =>{
    //Роут GET /, который будет возвращать главную HTML страницу со всеми заметками.
    app.get('/', async (req, res) => {
        try {
            res.render('index', {stylesheet: "css/style.css", notes: await db.collection('ToDoListCollection').find().toArray()});
        } catch (err) {
            console.log(err);
            return err;
        }
    })
};