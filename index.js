const express = require('express');
const MongoClient = require('mongodb').MongoClient;
const bodyParser = require('body-parser');
const path = require('path');

const app = express();
const uri = "mongodb+srv://admin:admin@todolist-z5jot.mongodb.net/ToDoList?retryWrites=true";
const host = 3000;

app.set('views', './views');
app.set('view engine', 'pug');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname,'/public')));

MongoClient.connect(
    uri,
    {useNewUrlParser: true},
    (err, client) => {
        if(err){
            return console.log(err);
        }
        const dataBase = client.db("ToDoList");
        require('./routes')(app, dataBase);
        app.listen(host, () => {
            console.log(`connected to ${uri}`);
            console.log(`We are on http://localhost:${host}`)
        })
    });


// //Роут GET /notes/${id}, который будет отдавать HTML страницу детального отображения заметки.
// app.get(`/notes/${id}`, (res, req) =>{
//
// });

// Роут PUT /api/notes/${id} для редактирования заметки.
// app.put(`/api/notes/${id}`, (res, req) =>{
//
// });
//
// //Роут DELETE /api/notes/${id} для удаления заметки.
// app.delete(`/api/notes/${id}`, (res, req) =>{
//
// });
//
// //Роут POST /api/notes для создания заметки.
// app.post('/api/notes', (res, req) =>{
//
// });
//
// //Роут GET /lists, который будет отдавать HTML страницу с формой создания списка.
// app.get('/lists', (res, req) =>{
//
// });
//
// //Роут GET /lists/${id}, который будет отдавать HTML страницу детального отображения списка.
// app.get(`/lists/${id}`, (res, req) =>{
//
// });
//
// //Роут GET /api/lists/${id} отображения заметки со списком.
// app.get(`/api/lists/${id}`, (res, req) =>{
//
// });
//
// //Роут POST /api/lists для добавления нового списка задач с учетом того, что количество позиций в списке - не ограничено и заранее не известно.
// app.post('/api/lists', (res, req) =>{
//
// });
//
// //Роут PUT /api/lists/${id} для редактирования списка задач.
// app.put(`/api/lists/${id}`, (res, req) =>{
//
// });
//
// //Роут DELETE /api/lists/${id} для удаления заметки со списком.
// app.delete(`/api/lists/${id}`, (res, req) =>{
//
// });