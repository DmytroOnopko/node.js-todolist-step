'use strict';
const MongoClient = require('mongodb').MongoClient;
const uri = "mongodb+srv://admin:admin@todolist-z5jot.mongodb.net/test?retryWrites=true";
const client = new MongoClient(uri, { useNewUrlParser: true });

async function checkCard(card){
    const cards = await getAllCards();
    return cards.some(item => (item.title === card.title));
}

async function addCard(card) {
    try {
        await client.connect();
        const collection = client.db("ToDoList").collection("ToDoListCollection");
        if(!(await checkCard(card))) {
            await collection.insertOne(card);
            console.log('card added');
            return true;
        }else{
            console.log('card: found');
            return false;
        }
    }catch(err) {
        console.log(err);
    }finally {
        client.close();
    }
}

async function removeCard (card){
    try {
        await client.connect();
        const collection = client.db("ToDoList").collection("ToDoListCollection");
        const res = await collection.findOneAndDelete(card);
        console.log(res);
        return (Boolean(res.value));
    } catch(err) {
        console.log(err);
    } finally {
        client.close();
    }
}

async function getAllCards() {
    try {
        await client.connect();
        const collection = client.db("ToDoList").collection("ToDoListCollection");
        return await collection.find().toArray();
    } catch(err) {
        console.log(err);
        return err;
    } finally {
        client.close();
    }
}

module.exports = {
    addCard,
    removeCard,
    getAllCards,
};