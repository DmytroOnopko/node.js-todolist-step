'use strict';
const MongoClient = require('mongodb').MongoClient;
const uri = "mongodb+srv://admin:admin@mydb-gsvok.mongodb.net/test?retryWrites=true";
const client = new MongoClient(uri, { useNewUrlParser: true });

async function connectDb (){
    try{
        await client.connect();
    }catch (e) {
        console.log(e);
    }finally {
        client.close();
    }
}

module.exports = connectDb;