const mainePage = require('./app/main/mainpage');
const addNotes = require('./app/notes/addNotes');
const removeNotes = require('./app/notes/removeNotes');
const showNotes = require('./app/notes/showNotes');


module.exports = (app, db) => {
    mainePage(app, db);
    addNotes(app, db);
    removeNotes(app, db);
    showNotes(app, db);
};